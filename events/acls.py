# this should contain the code I use to make http requests to pexels and to open weather map
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY

# we'll write two functions: one to make a request to the pexels api and one to make a request to the open weather api


def get_photo(city, state):
    headers = {
        "Authorization": PEXELS_API_KEY,
    }
    query = f"{city},{state}"
    url = f"https://api.pexels.com/v1/search?query={query}"
    response = requests.get(url, headers=headers)
    picture_url = response.json()["photos"][0]["src"]["original"]
    return {
        "picture_url": picture_url,
    }


def get_lat_long(location):
    # return lat and long from openweather api
    params = {
        "q": f"{location.city},{location.state.abbreviation},USA",
        "appid": OPEN_WEATHER_API_KEY,
    }
    url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(url, params)
    lat = response.json()[0]["lat"]
    lon = response.json()[0]["lon"]
    return {
        "lat": lat,
        "lon": lon,
    }


def get_weather_data(location):
    lat_lon = get_lat_long(location)
    if lat_lon is None:
        return None
    params = {
        "lat": lat_lon["lat"],
        "lon": lat_lon["lon"],
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    url = "http://api.openweathermap.org/data/2.5/weather"
    response = requests.get(url, params)
    description = response.json()["weather"][0]["description"]
    temp = response.json()["main"]["temp"]
    return {
        "description": description,
        "temp": temp,
    }
