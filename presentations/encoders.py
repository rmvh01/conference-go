from common.json import ModelEncoder
from events.api_views import ConferenceListEncoder
from .models import Presentation


class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "title",
    ]


class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
        "conference",
    ]
    encoders = {
        "conference": ConferenceListEncoder(),
    }
