# Generated by Django 4.0.3 on 2023-06-23 21:52

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0001_initial'),
        ('presentations', '0003_alter_presentation_conference'),
    ]

    operations = [
        migrations.AlterField(
            model_name='presentation',
            name='conference',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='presentations', to='events.conference'),
        ),
    ]
